---
geometry:
  - top=20mm
  - left=30mm
---

# Levers for GPU power/performance

All interesting parameters are available via nvidia-smi, no need to use NVML in custom program.

## nvidia-smi

### -ac, --applications-clock=MEM\_CLOCK,GRAPHICS\_CLOCK
Of the 4 clocks reported by `nvidia-smi` (graphics, SM, mem, video), this sets graphics/SM and memory _maximum_ clocks.
Graphics clock apparently equals SM clock (changing graphics impacts performance of SM, and reported values are identical).
Graphics/SM clock in [135-1530] MHz for gemini.
Memory clock is constant (877 MHz) for gemini.

-rac, --reset-applications-clocks
 
-acp, --applications-clocks-permission=MODE : 
Toggle  whether  applications  clocks  can be changed by all users or only by root.  Available arguments are 0|UNRESTRICTED, 1|RESTRICTED.

### -pl, --power-limit=
Software power limit, from 150-300W. Will throttle processors, and report.

### -lgc, --lock-gpu-clocks=MIN\_GPU\_CLOCK,MAX\_GPU\_CLOCK
Specifies <minGpuClock,maxGpuClock> clocks as a pair (e.g. 1500,1500) that defines closest desired locked GPU clock speed in MHz.
Input can also use be a singular desired clock value (e.g. <GpuClockValue>).  Supported on Volta+.  Requires root.
Supercedes applications clock.
Gpu clock means graphics/SM clock.
Drawback is that these limits are not reported with queries.

-rgc, --reset-gpu-clocks

## Notes on usage:

Shell commands to query limits and values:

- `nvidia-smi -i 0 --query-gpu=power.min_limit --format=csv,noheader,nounits` minimum enforceable power limit (ex: 150 W), `-i 0` select GPU 0.
- `nvidia-smi -i 0 --query-gpu=power.max_limit --format=csv,noheader,nounits` maximum enforceable power limit (ex: 300 W).
- `nvidia-smi -i 0 --query-gpu=power.limit --format=csv,noheader,nounits` current set power limit.
- `nvidia-smi --query-supported-clocks=gr --format=csv,noheader,nounits` settable application clock values for SM/graphics
- `nvidia-smi --query-supported-clocks=mem --format=csv,noheader,nounits` settable application clock values for memory
- `nvidia-smi -i 0 --query-gpu=clocks.max.gr --format=csv,noheader,nounits` maximum setable SM clock frequency (ex: 1530 MHz).
- `nvidia-smi -i 0 --query-gpu=clocks.max.mem --format=csv,noheader,nounits` maximum setable memory clock frequency (ex: 877 MHz).
- `nvidia-smi -i 0 --query-gpu=clocks.applications.gr --format=csv,noheader,nounits` current SM clock limit (ex: 1530 MHz).
- `nvidia-smi -i 0 --query-gpu=clocks.gr --format=csv,noheader,nounits` current SM clock (ex: 1530 MHz).

Other notes:

- oarsub request like `oarsub -l {gpu_count>1}/nodes=1/core=40/gpu=1,walltime=01:00 -n energumentest bash -l -c "./test_power.py"`
  + `gpu_count>1` for gemini nodes
  + `core=40` to reserve all cores for `sudo-g5k`
  + `bash -l` for login shell that sets path to cuda executables (otherwise use modules).
- cluster selection: `oarsub -p "wattmeter='YES' and cluster in ('graoully')" -l /cluster=1/nodes=4,walltime=5:0:0`
- to run commands as root from test suite:  `executor.local(cmd_as_root, root=True)`
- sudo-g5k : full node reservation
- `nvidia-smi -q -i 0` : basic report on all properties CL
- `nvidia-smi -q -i 0 -d CLOCK` shows clocks, but in a human readable format.
Choose from: MEMORY, UTILIZATION, ECC, TEMPERATURE, POWER, CLOCK, COMPUTE, PIDS, PERFORMANCE, SUPPORTED\_CLOCKS,  PAGE\_RETIREMENT,  ACCOUNTING,  ENCODER\_STATS
- or `--query-gpu=` (use with `--format=csv`) pstate (P0 to P12), temperature.memory, temperature.gpu, power.draw (+/-5W), power.limit (enforced.power.limit is not setable), power.min\_limit, power.max\_limit (150-300), clocks.current.graphics (shader) or clocks.gr, clocks.current.sm or clocks.sm, clocks.current.mem or clocks.mem,  clocks.video (encoder), clocks.applications.mem, clocks.applications.graphics (shader)

 
