#!/usr/bin/python3
"""This script run HPC benchmarks with enabled monitoring

"""

import os

import experiment

from benchSuites import sleepbench, cpubench, mpibench, membench, npbbench
from leverages import dvfs, pct
from monitors import mojitos


BASEDIR = os.path.dirname(os.path.abspath(__file__))

BENCHMARKS = [sleepbench.SleepBench(),
              cpubench.CpuBench(),
              mpibench.MpiBench(),
              membench.MemBench(),
              npbbench.NpbBench()]

#BENCHMARKS = [sleepbench.SleepBench(10),
#              npbbench.NpbBench({'is'})]

LEVERAGES = [dvfs.Dvfs(BASEDIR),
             pct.Pct(BASEDIR, baseline=True)]

MONITORS = [mojitos.Mojitos()]

experiment.run_experiment("trash", BENCHMARKS, basedir=BASEDIR,
                          leverages=LEVERAGES, monitors=MONITORS, sweep=True)
