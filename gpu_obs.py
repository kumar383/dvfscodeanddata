#!/usr/bin/python3
"""This script run HPC benchmarks with enabled monitoring

"""

import os
import experiment

from benchSuites import gpucpubench, gpumembench
from leverages import gpuclock, gpupow, dvfs
from monitors import power, powergpu

BASEDIR = os.path.dirname(os.path.abspath(__file__))
BENCHMARKS = [gpucpubench.GpuCpuBench()]
MONITORS = [powergpu.Power()]

print('4 power x 4 clock capping: gpu-pow4clock4')
LEVERAGES = [gpupow.GpuPower(steps=4), gpuclock.GpuClock(steps=4)]
experiment.run_experiment("gpu-pow4clock4", BENCHMARKS, basedir=BASEDIR, leverages=LEVERAGES, monitors=MONITORS, sweep=True, times=1)

print('15 clock capping: gpu-clock')
LEVERAGES = [gpuclock.GpuClock(steps=15)]
experiment.run_experiment("gpu-clock15", BENCHMARKS, basedir=BASEDIR, leverages=LEVERAGES, monitors=MONITORS, sweep=True, times=1)

print('16 power capping: gpu-pow16')
LEVERAGES = [gpupow.GpuPower(steps=16)]
experiment.run_experiment("gpu-pow16", BENCHMARKS, basedir=BASEDIR, leverages=LEVERAGES, monitors=MONITORS, sweep=True, times=1)

print('4 clock x 2 DVFS capping: gpu-clock4dvfs2')
LEVERAGES = [gpuclock.GpuClock(steps=4), dvfs.Dvfs(BASEDIR, dummy=True)]
experiment.run_experiment("gpu-clock4dvfs2", BENCHMARKS, basedir=BASEDIR, leverages=LEVERAGES, monitors=MONITORS, sweep=True, times=1)

