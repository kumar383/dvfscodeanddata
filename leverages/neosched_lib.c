/*******************************************************
 Copyright (C) 2018-2019 Georges Da Costa <georges.da-costa@irit.fr>

    This file is part of Mojitos.

    Mojitos is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Mojitos is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Mojitos.  If not, see <https://www.gnu.org/licenses/>.

*******************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>

#include "neosched_lib.h"

void set_pct(int value) {
  FILE* fid = fopen("/sys/devices/system/cpu/intel_pstate/max_perf_pct", "w");
  fprintf(fid, "%d", value);
  fclose(fid);
}
char **freq_filenames;
void init_dvfs() {
  int res = 0;
  int nb_cores = 0;
  char buffer[100];
  while(res == 0) {
    nb_cores++;
    sprintf(buffer, "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq", nb_cores);
    res = access(buffer, W_OK);
  }
  freq_filenames = malloc(sizeof(char*)*(nb_cores+1));
  for(int i=0; i<nb_cores; i++) {
    freq_filenames[i] = malloc(sizeof(char)*100);
    sprintf(freq_filenames[i], "/sys/devices/system/cpu/cpu%d/cpufreq/scaling_max_freq", i);
  }
  freq_filenames[nb_cores]=0;
}

void set_freq(int value) {
  for(int i=0; freq_filenames[i]!=0; i++) {
    FILE* fid = fopen(freq_filenames[i], "w");
    fprintf(fid, "%d", value);
    fclose(fid);
  }
}

int freq = 0;
int target = 0;
//                                4          4                   6           10
//void choose_frequency(long long counter, long long network, long rapl, long long system) {

void choose_frequency_rapl(uint64_t *new_rapl, uint64_t *old_rapl) {
  if (new_rapl[2]-old_rapl[2] + new_rapl[5]-old_rapl[5] > 3000000)
    target = 1300000;
  else
    target = 2300000;
  if (target != freq) {
    set_freq(target);
    freq = target;
  }
}

void choose_frequency_network(long *new_network, long *old_network) {
  if (new_network[2]-old_network[2] + new_network[5]-old_network[5] > 1500000)
    target = 1300000;
  else
    target = 2300000;
  if (target != freq) {
    set_freq(target);
    freq = target;
  }
}
