from leverages import dvfs
from monitors import mojitos

class NeoSched(dvfs.Dvfs):
    
    def build(self, executor):
        dvfs.Dvfs.build(self, executor)
        self.available_frequencies.append(0)
        moj = mojitos.Mojitos()
        moj.build(executor)
        executor.local('cp -f %s/leverages/neosched_* /tmp/mojitos/' % self.basedir)
        executor.local('cd /tmp/mojitos; make -f neosched_mak')
        executor.local('mkdir -p /tmp/neosched')
        executor.local('cp -f /tmp/mojitos/neosched /tmp/neosched/')
        executor.sync('/tmp/neosched')

    def start(self, freq):
        if freq != 0:
            dvfs.Dvfs.start(self, freq)
        else:
            self.executor.hosts('/tmp/neosched/neosched -f 10 -d %s &' % self.executor.get_network_if())

    def stop(self):
        self.executor.hosts('killall neosched')
    
    def get_state(self):
        if self.defi:
            return (0, 0)
        else:
            return dvfs.Dvfs.get_state(self)
