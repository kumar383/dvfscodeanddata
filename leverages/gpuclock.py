
class GpuClock:
    'nvidia-smi graphics/sm clock setting'
    def __init__(self, dummy=False, baseline=False, steps=2):
        self.dummy = dummy
        self.baseline = baseline
        self.executor = None
        self.available_frequencies = []
        self.available_frequencies_mem = []
        self.clock_mem_max = None
        self.clock_sm_min = None
        self.nsteps = steps

    def build(self, executor):
        'Gather the available frequencies'
        self.executor = executor
        q = "nvidia-smi --query-supported-clocks=gr --format=csv,noheader,nounits | tr '\n' ' '"
        clk_s = self.executor.local(q)
        clk = sorted([int(f) for f in clk_s.strip().split(' ')])
        self.available_frequencies = [clk[(i*(len(clk)-1))//(self.nsteps-1)] for i in range(self.nsteps)]
        self.clock_sm_min = clk[0]
        q = "nvidia-smi --query-supported-clocks=mem --format=csv,noheader,nounits | tr '\n' ' '"
        clk_s = self.executor.local(q)
        self.available_frequencies_mem = sorted([int(f) for f in clk_s.strip().split(' ')])
        self.clock_mem_max = self.available_frequencies_mem[-1]
        if self.dummy:
            self.available_frequencies = [self.available_frequencies[0], self.available_frequencies[-1]]
        if self.baseline:
            self.available_frequencies = [self.available_frequencies[-1]]

    def available_states(self):
        'Returns all the frequencies'
        return self.available_frequencies

    def start(self, freq):
        'Sets the right frequency range on gpu 0'
        if freq in self.available_frequencies:
            self.executor.local('nvidia-smi -i 0 -ac  %s,%s' % (self.clock_mem_max, freq), root=True)

    def stop(self):
        'Reverts to the maximum frequency'
        self.executor.local('nvidia-smi -i 0 -rac', root=True)
            
    def get_state(self):
        'Returns the current min and max application frequencies'
        cur_min = self.clock_sm_min
        cur_max = int(self.executor.local('nvidia-smi -i 0 --query-gpu=clocks.applications.gr --format=csv,noheader,nounits'))
        return (cur_min, cur_max, self.clock_mem_max)

    def state_to_str(self):
        'Returns the current min and max frequencies as a string'
        (cur_min, cur_max, mem_max) = self.get_state()
        return '%s %s %s' % (cur_min, cur_max, mem_max)

    def get_labels(self):
        'Returns the label for frequencies'
        return ('fmin', 'fmax', 'fmemax')

