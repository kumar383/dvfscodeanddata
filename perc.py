#!/usr/bin/python3
"""This script run HPC benchmarks with enabled monitoring

"""

import os
import experiment

from leverages import dvfs
from monitors import mojitos
from benchSuites import percentagebench

if __name__ == "__main__":
    BASEDIR = os.path.dirname(os.path.abspath(__file__))

    BENCHMARKS = [percentagebench.PercentageBench(values=range(1, 110, 5), duration=30)]
    LEVERAGES = [dvfs.Dvfs(BASEDIR)]
    MONITORS = [mojitos.Mojitos()]

    experiment.run_experiment("perc", BENCHMARKS, basedir=BASEDIR,
                              leverages=LEVERAGES, monitors=MONITORS, sweep=True, times=10)
