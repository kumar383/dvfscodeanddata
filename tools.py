import json
from statistics import median
import shutil
import os
import bisect

import requests
from IPython.display import clear_output



def find_le(elements, value):
    'Find rightmost value in elements less than or equal to value'
    position = bisect.bisect_right(elements, value)
    return elements[position-1]

def get_power_local(start, end, debug=False):
    'To use with a local powermeter link in laptops'
    directory = 'local_powermeter'
    file_list = sorted([int(filename) for filename in os.listdir(directory)])
    file_name = find_le(file_list, start)

    with open(directory+'/'+str(file_name)) as file_id:
        raw_data = [line.split() for line in file_id.readlines()]
    power_data = [(int(timestamp), float(power)) for (timestamp, power) in raw_data
                  if int(timestamp) >= start and int(timestamp) <= end]

    timestamps, values = zip(*power_data)

    res = {"items":[{"timestamps":timestamps, "values":values}]}
    data = json.dumps(res)
    if debug:
        print(data)
    return data

def get_power_g5k(host, site, start, end, password, debug):
    'Gets the power from nodes of Grid5000'
    request = 'https://api.grid5000.fr/stable/sites/%s/metrics/power/timeseries?resolution=1&only=%s&from=%s&to=%s' % (site, host, start, end)
    if debug:
        print(request)
        print(password)
    res = requests.get(request, auth=password)

    data = res.text
    if debug:
        print(data)
    if res.status_code != 200:
        print("problem to access api.grid5000.fr")
        print(res.text)
        return None
    return data


from cryptography.fernet import Fernet
import getpass

def get_g5k_password():
    try:
        with open('/home/gdacosta/.gdacosta/g5k.key', 'rb') as file:
            key = b'lsB2BLQ9vZ1uVMu7EwW6jTgzX8RyM1bA3obHNbPyWOE='
            f = Fernet(key)
            secret=file.read()
        return ('gdacosta', f.decrypt(secret).decode())
    except:
        return (input('Login on Grid5000'), getpass.getpass('password on Grid5000'))

def get_power(hostname, start, end, password='', debug=False, cachedir='cache', copydest=None):
    'Generic method to obtain the power of a benchmark, either on g5k or local'
    try:
        host, site, _, _ = hostname.split('.')
    except:
        host, site = hostname, 'local'

    if debug:
        print('Get_Power', hostname, start, end)
    try:
        source = '%s/%s_%s_%s_%s' %(cachedir, host, site, start, end)
        if debug:
            print(source)
        with open(source, 'r') as file:
            data = file.read()
        if debug:
            print('file found')
        if not copydest is None:
            shutil.copy(source, copydest)
            if debug:
                print('file copied in ', copydest)
    except:
        if site == 'local':
            data = get_power_local(start, end, debug)
        else:
            data = get_power_g5k(host, site, start, end, password, debug)

    if not data is None:
        with open('%s/%s_%s_%s_%s' %(cachedir, host, site, start, end), 'w') as file:
            file.write(data)

    raw = json.loads(data)['items'][0]
    timestamps = raw['timestamps']
    values = raw['values']
    if len(values) == 0:
        return None
    return timestamps, values

def power_to_energy(timestamps, power):
    'Approximates missing timestamps'
    mu = median([timestamps[i+1]-timestamps[i] for i in range(len(timestamps)-1)])

    energy = power[0] * mu/2

    for tidx in range(len(timestamps)-1):
        delta = timestamps[tidx+1]-timestamps[tidx]
        energy += (power[tidx]+power[tidx+1])*(delta)/2

    energy += power[-1]*mu/2
    return energy

def get_energy(hostname, start_time, end_time, password='', debug=False):
    'Retunrs the energy consumed during an experiment'
    (timestamps, values) = get_power(hostname, start_time, end_time, password, debug)
    return power_to_energy(timestamps, values)

def update_progress(progress):
    'progress bar for jupyter notebook'
    bar_length = 20
    if isinstance(progress, int):
        progress = float(progress)
    if not isinstance(progress, float):
        progress = 0
    if progress < 0:
        progress = 0
    if progress >= 1:
        progress = 1

    block = int(round(bar_length * progress))

    clear_output(wait=True)
    text = "Progress: [{0}] {1:.1f}%".format("#"*block + "-"*(bar_length - block), progress*100)
    print(text)

def clean_dataframe(dataframe):
    'Removes unuseful values'
    dataframe = dataframe.dropna(axis='columns').drop(["#timestamp"], axis=1)
    return dataframe

def normalize_dataframe(dataframe):
    'removes unuseful values and normalize the remaining values between 0 and 1'
    tmp = (dataframe-dataframe.min())/(dataframe.max()-dataframe.min())
    return clean_dataframe(tmp)
