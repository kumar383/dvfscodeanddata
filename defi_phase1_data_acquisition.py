#! /usr/bin/python3

from execo import *
import os
import random
import time
import sys
from functools import reduce

# Global
def read_int(filename):
    with open(filename) as f:
        return int(f.readline())

def exec_shell(cmd, shell=True):
    proc = Process(cmd, shell=shell)
    proc.run()
    return proc.stdout

def exec_mpi(cmd, hosts):
    (hostfile, nb) = hosts
    return exec_shell('mpirun --mca orte_rsh_agent oarsh -np %s --machinefile %s %s' %(nb, hostfile, cmd))

# Init MPI
def init_node_files(nodeFile, mpiNodeFile):
    with open(os.environ['OAR_NODE_FILE']) as f:
        content = [host.strip() for host in f.readlines()]
        hostnames = reduce(lambda l, x: l if x in l else l+[x], content, [])
        nbproc=len(content)
    nb=len(hostnames)

    with open(nodeFile, 'w') as f:
        for host in hostnames:
            f.write(host+" slots=1\n")

    with open(mpiNodeFile, 'w') as f:
        for host in hostnames:
            f.write(host+" slots=%s\n" % (nbproc//nb))
    return hostnames, nbproc

# Init MojitO/S
def init_mojitos():
    exec_shell('sudo-g5k apt install -y libpowercap0 libpowercap-dev powercap-utils')
    exec_shell('cd /tmp; git clone https://git.renater.fr/anonscm/git/mojitos/mojitos.git')
    exec_shell('cd /tmp/mojitos; make')
    exec_shell("sudo sh -c 'echo 0 >/proc/sys/kernel/perf_event_paranoid'")
    exec_shell("sudo chmod a+w /sys/class/powercap/intel-rapl/*/*")
    exec_shell("sudo chmod a+w /sys/class/powercap/intel-rapl/*/*/*")

# Init DVFS
def get_dvfs_values():
    fmin=read_int('/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_min_freq')
    boost=read_int('/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq')
    fmax=read_int('/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq')
    nbpstate=read_int('/sys/devices/system/cpu/intel_pstate/num_pstates')
    delta=(boost-fmin) // (nbpstate-1)
    nbpct=(fmax - fmin) // delta
    pctmin=fmin * 100 // boost
    pctmax=fmax * 100 // boost

    FREQ=range(fmin, fmax, delta)
    PCT = [(pctmax*i + pctmin*(nbpct-i))//nbpct for i in range(0, nbpct+1)]
    return FREQ, PCT


################################
# NPB benches
#
NPB_ref_set = {'ep', 'bt', 'sp', 'cg', 'is', 'ft', 'lu', 'mg'}
def run_bench_NPB(bench, params, MPInodeFile):
    classtype, nbproc = params
    execution_time=exec_shell("mpirun --mca orte_rsh_agent oarsh -np %s --machinefile %s /tmp/bin/%s.%s.x | grep 'Time in seconds' | awk '{print $5}'" %(nbproc, MPInodeFile, bench, classtype))
    benchname=bench+'-'+classtype+'-'+str(nbproc)
    return execution_time.strip(), benchname
#
def build_bench_NPB(benchs, nbproc):
    exec_shell('tar xfC /home/gdacosta/NPB3.4-MPI.tgz /tmp/', shell=False)
    exec_shell('cp /tmp/NPB3.4-MPI/config/make.def.template /tmp/NPB3.4-MPI/config/make.def ')

    if nbproc==64:
        params = {'ep':'D', 'bt':'C', 'sp':'C', 'cg':'D', 'is':'D', 'ft':'D', 'lu':'C', 'mg':'D'}
    elif nbproc==256:
        params = {'ep':'D', 'bt':'D', 'sp':'D', 'cg':'D', 'is':'E', 'ft':'E', 'lu':'D', 'mg':'D'}
    else: # for 16
        params = {'ep':'C', 'bt':'B', 'sp':'B', 'cg':'C', 'is':'D', 'ft':'C', 'lu':'B', 'mg':'D'}

    for bench in set(benchs) & NPB_ref_set:
        exec_shell('cd /tmp/NPB3.4-MPI/; make %s CLASS=%s' %(bench, params[bench]))

    exec_shell('cp /tmp/NPB3.4-MPI/bin/* /tmp/bin/')

    for key in params:
        params[key] = [(params[key], nbproc)]
    return params

################################
# mem bench
#
mem_ref_set = {'mem'}
def build_bench_mem(bench, nbproc):
    exec_shell('mpicc -O /home/gdacosta/mem.c -o /tmp/bin/mem')
    params = {'mem':list(range(1,2*nbproc+1))}
    #params = {'mem':list(range(1,4))}
    return params

def run_bench_mem(bench, params, mpiNodeFile):
    nbproc=params
    execution_time=exec_shell("mpirun --use-hwthread-cpus -np %s /tmp/bin/mem" % nbproc)
    return execution_time.strip(), 'mem-%02d' % nbproc

################################
# multi bench
#
mpi_ref_set = {'mpi'}
def build_bench_mpi(benchs, nproc):
    exec_shell('mpicc /home/gdacosta/mpi_generic.c -o /tmp/bin/mpi_generic')
    if nproc==16:
        params = {'mpi':[(nproc,10,0,0), (nproc,0,15,0), (nproc,0,0,100), (nproc,2,3,20)]}
    else:
        params = {'mpi':[(nproc,30,0,0), (nproc,0,30,0), (nproc,0,0,2), (nproc,15,15,1)]}

    return params

def run_bench_mpi(bench, params, MPInodeFile):
    nproc, pcpu, pmem, pmpi = params
    delta = exec_mpi('/tmp/bin/mpi_generic -c %s -m %s -n %s' %(pcpu, pmem, pmpi), (MPInodeFile, nproc)).strip()
    return delta, 'mpigeneric-%s-%s-%s' % (pcpu, pmem, pmpi)


################################
# cpu bench
#
cpu_ref_set = {'cpu'}
def build_bench_cpu(benchs, nproc):
    exec_shell('mpicc /home/gdacosta/cpu.c -o /tmp/bin/cpu')
    ref=92000
    time_ref=30
    params = {'cpu':[(nproc, ref, time_ref), (nproc, ref//4, time_ref), (nproc, ref//2, time_ref), (nproc, ref//8, time_ref)]}
    return params

def run_bench_cpu(bench, params, MPInodeFile):
    (nproc, load, length) = params
    delta=exec_mpi('/tmp/bin/cpu %s %s' % (load, length), (MPInodeFile, nproc)).strip()
    return delta, 'cpu-%s-%s' %(load, length)
################################
# sleep bench
#
sleep_ref_set = {'sleep'}
def build_bench_sleep(benchs, nbproc):
    return {'sleep':[30]}

def run_bench_sleep(bench, params, MPInodeFile):
    time.sleep(params)
    return str(params), 'sleep-'+str(params)
#  ___              _    ___      _ _            _
# | _ ) ___ _ _  __| |_ / __|_  _(_) |_ ___   __| |__ _ ______
# | _ \/ -_) ' \/ _| ' \\__ \ || | |  _/ -_) / _| / _` (_-<_-<
# |___/\___|_||_\__|_||_|___/\_,_|_|\__\___| \__|_\__,_/__/__/
class BenchSuite:
    def __init__(self, ref_set, run, build):
        self.names = ref_set
        self.run = run
        self.build = build

################################
# Experiment
#
class Experiment:
    benchSuites=[]
    params={}
    def __init__(self, name, dvfs=True):
        self.MPInodeFile = '/dev/shm/managed_nodes'
        self.nodeFile = '/dev/shm/nodes'

        init_mojitos()

        self.hostnames, self.nbproc = init_node_files(self.nodeFile, self.MPInodeFile)

        self.idexpe=random.randint(0, 65536)
        self.outputFile = '%s_%s_%s' %(name, self.hostnames[0], self.idexpe)
        self.IF=exec_shell('ip addr | grep master | tr : " "').strip().split()[1]

        self.dvfs = dvfs
        self.dvfs_init()
        self.FREQ, self.PCT = get_dvfs_values()

    def dvfs_get_available_freq(self):
        return self.FREQ

    def dvfs_init(self):
        if self.dvfs:
            exec_mpi('/home/gdacosta/manager.sh init', (self.nodeFile,len(self.hostnames)))

    def dvfs_set_freq(self, freq):
        if self.dvfs and not freq is None:
            exec_mpi('/home/gdacosta/manager.sh freq '+str(freq), (self.nodeFile,len(self.hostnames)))

    def add_bench(self, benchSuite):
        self.benchSuites.append(benchSuite)

    def run_bench(self, bench, param):
        for benchSuite in self.benchSuites:
            if bench in benchSuite.names:
                return benchSuite.run(bench, param, self.MPInodeFile)

    def build_bench(self, benchlist):
        os.makedirs('/tmp/bin/', exist_ok=True)
        for benchSuite in self.benchSuites:
            current_benchs = set(benchlist) & benchSuite.names
            if len(current_benchs) > 0:
                self.params.update(benchSuite.build(current_benchs, self.nbproc))

        for host in self.hostnames:
            exec_shell('oarcp -r /tmp/bin %s:/tmp/' % (host))

    def get_all_benchnames(self):
        return set().union(*[suite.names for suite in self.benchSuites])

    def get_params(self, bench):
        return self.params[bench]

    def get_freq_pct(self):
        cur_min_freq = read_int('/sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq')
        cur_max_freq = read_int('/sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq')
        cur_min_pct = read_int('/sys/devices/system/cpu/intel_pstate/min_perf_pct')
        cur_max_pct = read_int('/sys/devices/system/cpu/intel_pstate/max_perf_pct')
        return (cur_min_freq,cur_max_freq,cur_min_pct, cur_max_pct)

    def monitor_bench(self, bench, param):
        output_time=''
        cur_min_freq, cur_max_freq, cur_min_pct,cur_max_pct = self.get_freq_pct()
        print('starting ', self.outputFile, bench, param, 'at',cur_max_freq)
        while output_time=='':
            time.sleep(5)
            beg_time = int(time.time())
            exec_shell('/tmp/mojitos/mojitos -t 0 -f 10 -d %s -p branch_misses,cache_l1d,page_faults,branch_instructions -r -o /dev/shm/monitoring &' % (self.IF))
            output_time, benchname = self.run_bench(bench, param)
            exec_shell('killall mojitos')
            end_time = int(time.time())
        result = '%s %s %s %s %s %s %s %s %s %s ' % (self.hostnames[0], benchname, self.nbproc, output_time, beg_time, end_time, cur_min_freq, cur_max_freq, cur_min_pct, cur_max_pct)
        result += ';'.join(self.hostnames)
        result += '\n'
        with open(self.outputFile, 'a') as f:
            f.write(result)
        filename_moj = self.outputFile+'_mojitos'
        os.makedirs(filename_moj, exist_ok=True)

        exec_shell('mv /dev/shm/monitoring %s/%s_%s_%s' % (filename_moj, self.hostnames[0], benchname, beg_time))


expe = Experiment("mem_phaseOne")
expe.add_bench(BenchSuite(NPB_ref_set, run_bench_NPB, build_bench_NPB))
expe.add_bench(BenchSuite(sleep_ref_set, run_bench_sleep, build_bench_sleep))
expe.add_bench(BenchSuite(cpu_ref_set, run_bench_cpu, build_bench_cpu))
expe.add_bench(BenchSuite(mpi_ref_set, run_bench_mpi, build_bench_mpi))
expe.add_bench(BenchSuite(mem_ref_set, run_bench_mem, build_bench_mem))

benchNames=expe.get_all_benchnames()
availableFrequencies=expe.dvfs_get_available_freq()

#benchNames={'mpi', 'mg', 'sleep', 'cpu', 'ep', 'cg', 'sp'}
availableFrequencies=[availableFrequencies[0], availableFrequencies[-1]]
#availableFrequencies = [None]
benchNames={'sleep'}

expe.build_bench(benchNames)

for freq in availableFrequencies:
    expe.dvfs_set_freq(freq)
    for bench in benchNames:
        for param in expe.get_params(bench):
            expe.monitor_bench(bench, param)
