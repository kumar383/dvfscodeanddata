#!/usr/bin/python3
"""This script run HPC benchmarks with enabled monitoring

"""

import os
import experiment

from benchSuites import gpucpubench, gpumembench
from leverages import gpuclock, gpupow
from monitors import power, powergpu

BASEDIR = os.path.dirname(os.path.abspath(__file__))

#BENCHMARKS = [gpucpubench.GpuCpuBench(), gpumembench.GpuMemBench()]
BENCHMARKS = [gpucpubench.GpuCpuBench()]
LEVERAGES = [gpupow.GpuPower()]
LEVERAGES2 = [gpuclock.GpuClock(steps=4)]
MONITORS = [powergpu.Power()]

print('power benchs')
experiment.run_experiment("gpu-power", BENCHMARKS, basedir=BASEDIR, leverages=LEVERAGES, monitors=MONITORS, sweep=True, times=1)
#print('clock benchs')
#experiment.run_experiment("gpu-clock", BENCHMARKS, basedir=BASEDIR, leverages=LEVERAGES2, monitors=MONITORS, sweep=True, times=1)
