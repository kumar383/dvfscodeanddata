#! /usr/bin/python3

import sys, os
import importlib
import inspect
import experiment

if len(sys.argv) == 1:
    print("""Usage %s benchmarkname
Example : ./%s membench""" % (sys.argv[0], sys.argv[0]))
    sys.exit(0)

target = 'benchSuites.'+sys.argv[1]
bench_module = importlib.import_module(target, package=None)
_, bench_class = inspect.getmembers(bench_module, inspect.isclass)[0]

benchmark = bench_class()

executor = experiment.Executor()
basedir = os.path.dirname(os.path.abspath(__file__))
os.makedirs('/tmp/bin/', exist_ok=True)
params = benchmark.build(executor, basedir=basedir, deterministic=True)

print(benchmark.names)
print(params)
benchmark_name = benchmark.names.pop()
benchmark_params = params[benchmark_name].pop(0)
if callable(benchmark_params):
    benchmark_params = benchmark_params()
res=benchmark.run(benchmark_name, benchmark_params, executor)

print(res)
