#!/usr/bin/python3
"""This script run HPC benchmarks with enabled monitoring

"""

import os

import experiment

from benchSuites import sleepbench, cpubench, mpibench, membench, npbbench
from leverages import dvfs, pct, neosched
from monitors import mojitos


BASEDIR = os.path.dirname(os.path.abspath(__file__))

# BENCHMARKS = [sleepbench.SleepBench(),
#               cpubench.CpuBench(),
#               mpibench.MpiBench(),
#               membench.MemBench(),
#               npbbench.NpbBench()]

#BENCHMARKS = [sleepbench.SleepBench(10),
#              npbbench.NpbBench({'is'})]
#BENCHMARKS = [sleepbench.SleepBench(10)]

BENCHMARKS = [npbbench.NpbBench(options={'ep':'D', 'bt':'D', 'sp':'D', 'cg':'D',
                                         'is':'E', 'ft':'D', 'lu':'D', 'mg':'E'}
)]
LEVERAGES = [neosched.NeoSched(basedir=BASEDIR, baseline=True, specific_frequencies=[0, 1, 5])]
MONITORS = []

experiment.run_experiment("neosched", BENCHMARKS, basedir=BASEDIR,
                          leverages=LEVERAGES, monitors=MONITORS, sweep=True, times=2)
