#!/usr/bin/python3
"""This script run HPC benchmarks with enabled monitoring

"""

import os

import experiment

from benchSuites import sleepbench, cpubench, mpibench, membench, npbbench
from monitors import mojitos, power
from leverages import dvfs

BASEDIR = os.path.dirname(os.path.abspath(__file__))

BENCHMARKS = [mpibench.MpiBench(params=[(16,0,0,0), (0,16,0,0), (0,0,16,0),
                                        (0,0,0,16), (4,4,4,4), (0,0,8,8)]),
              membench.MemBench(param_func=lambda nb:[nb//2, nb]),
              npbbench.NpbBench(),
              sleepbench.SleepBench()]

LEVERAGES = [dvfs.Dvfs(basedir=BASEDIR)]


hw_pct = mojitos.get_names() ## All available sensors
MONITORS = [mojitos.Mojitos(sensor_set = hw_pct),
            power.Power()]

experiment.run_experiment("features", BENCHMARKS, basedir=BASEDIR,
                          leverages=LEVERAGES, monitors=MONITORS, sweep=True, times=5)
